# Wordpress-Shopee

To bring up the Wordpress, simply use this command:
```
docker-compose up -d
```

To scale the number of the Wordpress container, use this command:
```
docker-compose up -d --scale wordpress={NUM}
```

Input http://localhost:8000 to access in a web browser